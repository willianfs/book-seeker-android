package com.ciandt.book.seeker.ui.books.search

sealed class ScreenState {
    object Loading : ScreenState()
    object Error : ScreenState()
}
