package com.ciandt.book.seeker.data.network

import com.ciandt.book.seeker.data.model.book.BookResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface APIService {
    @GET("search")
    suspend fun getBookByTerm(@Query("term") term: String): BookResponse
}
