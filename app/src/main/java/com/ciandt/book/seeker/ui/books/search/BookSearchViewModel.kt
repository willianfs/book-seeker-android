package com.ciandt.book.seeker.ui.books.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ciandt.book.seeker.data.model.book.Book
import com.ciandt.book.seeker.data.model.book.toBook
import com.ciandt.book.seeker.data.repository.BookRepository
import kotlinx.coroutines.launch

private const val SEARCH_DEFAULT = "kotlin"

class BookSearchViewModel(private val bookRepository: BookRepository) : ViewModel() {

    private val _bookViewState = MutableLiveData<BookViewState>()
    val bookViewState: LiveData<BookViewState> = _bookViewState

    private val _screenState = MutableLiveData<ScreenState>()
    val screenState: LiveData<ScreenState> = _screenState

    private val _history = MutableLiveData<List<String>>()
    val history: LiveData<List<String>> = _history

    fun searchBookByTerm(term: String = SEARCH_DEFAULT) {
        _screenState.value = ScreenState.Loading

        viewModelScope.launch {
            try {
                val books = bookRepository.getBookByTerm(term)
                    .map { bookResult -> bookResult.toBook() }
                handleBookResult(books)
            } catch (exception: Throwable) {
                _screenState.value = ScreenState.Error
            }
        }
    }

    fun getHistory() {
        _history.value = bookRepository.getHistory()
    }

    fun fetchDefaultBooks() {
        searchBookByTerm(SEARCH_DEFAULT)
    }

    private fun handleBookResult(books: List<Book>) {
        if (books.isEmpty()) {
            _bookViewState.value = BookViewState.EmptyList
        } else {
            _bookViewState.value = BookViewState.Books(books)
        }
    }

    fun saveHistory(query: String) {
        bookRepository.saveHistory(query)
    }
}
