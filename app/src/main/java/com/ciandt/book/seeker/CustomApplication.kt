package com.ciandt.book.seeker

import android.app.Application
import com.ciandt.book.seeker.di.bookModule
import com.ciandt.book.seeker.di.networkModule
import com.orhanobut.hawk.Hawk
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CustomApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@CustomApplication)
            modules(
                listOf(
                    networkModule,
                    bookModule
                )
            )
        }
        Hawk.init(this)
            .build()
    }
}
