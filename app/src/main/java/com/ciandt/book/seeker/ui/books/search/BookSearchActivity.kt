package com.ciandt.book.seeker.ui.books.search

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.data.model.book.Book
import com.ciandt.book.seeker.ui.books.detail.BookDetailActivity
import com.miguelcatalan.materialsearchview.MaterialSearchView
import kotlinx.android.synthetic.main.activity_book_search.*
import org.koin.android.viewmodel.ext.android.viewModel

class BookSearchActivity : AppCompatActivity() {

    private val bookSearchViewModel by viewModel<BookSearchViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_book_search)
        setSupportActionBar(toolbar)

        setupViewModel()
        setupObservable()
        setupListeners()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val item = menu.findItem(R.id.actionSearch)
        searchView.setMenuItem(item)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.actionSearch -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupListeners() {
        searchView.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                bookSearchViewModel.saveHistory(query)
                bookSearchViewModel.searchBookByTerm(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })

        searchView.setOnSearchViewListener(object : MaterialSearchView.SearchViewListener {
            override fun onSearchViewClosed() {
            }

            override fun onSearchViewShown() {
                bookSearchViewModel.getHistory()
            }
        })

        textViewErrorInfo.setOnClickListener {
            viewFlipperBookSearch.displayedChild = RESULT_STATE
        }

        textViewEmptyState.setOnClickListener {
            viewFlipperBookSearch.displayedChild = RESULT_STATE
        }
    }

    private fun setupViewModel() {
        bookSearchViewModel.fetchDefaultBooks()
    }

    private fun setupObservable() = with(bookSearchViewModel) {
        bookViewState.observe(this@BookSearchActivity, Observer { bookState ->
            when (bookState) {
                is BookViewState.Books -> {
                    showBooks(bookState.books)
                }
                is BookViewState.EmptyList -> {
                    showState(EMPTY_STATE)
                }
            }
        })

        screenState.observe(this@BookSearchActivity, Observer { screenState ->
            when (screenState) {
                is ScreenState.Loading -> {
                    showState(LOADING_STATE)
                }

                is ScreenState.Error -> {
                    showState(ERROR_STATE)
                }
            }
        })

        history.observe(this@BookSearchActivity, Observer { history ->
            history?.let {
                searchView.setSuggestions(history.toTypedArray())
            }
        })
    }

    private fun goToBookDetails(book: Book) {
        startActivity(BookDetailActivity.newIntent(this, book))
    }

    private fun showState(state: Int) {
        viewFlipperBookSearch.displayedChild = state
    }

    private fun showBooks(books: List<Book>) {
        with(recyclerViewBooks) {
            adapter = BookSearchAdapter(books) { books ->
                goToBookDetails(books)
            }
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }
        viewFlipperBookSearch.displayedChild = RESULT_STATE
    }

    companion object {
        private const val LOADING_STATE = 0
        private const val RESULT_STATE = 1
        private const val EMPTY_STATE = 2
        private const val ERROR_STATE = 3
    }
}
