package com.ciandt.book.seeker.di

import com.ciandt.book.seeker.BuildConfig
import com.ciandt.book.seeker.data.network.APIService
import com.ciandt.book.seeker.BuildConfig.BASE_URL
import com.ciandt.book.seeker.data.repository.BookRepository
import com.ciandt.book.seeker.data.repository.BookRepositoryImpl
import com.ciandt.book.seeker.ui.books.search.BookSearchViewModel
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single { providerRetrofit(get()) }

    factory { provideAPIService(get()) }
    factory { provideOkhttpClient(get()) }
    factory { provideHttpLoggingInterceptor() }
}

val bookModule = module {
    viewModel { BookSearchViewModel(get()) }
    factory<BookRepository> { BookRepositoryImpl(get()) }
}

private fun providerRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

private fun provideAPIService(retrofit: Retrofit): APIService {
    return retrofit.create(APIService::class.java)
}

private fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
    return HttpLoggingInterceptor().setLevel(
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
    )
}

private fun provideOkhttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor)
        .addInterceptor { chain: Interceptor.Chain ->
            val newUrl = chain.request().url().newBuilder()
                .addQueryParameter("entity", "ibook")
                .build()
            val request = chain.request().newBuilder().url(newUrl).build()
            chain.proceed(request)
        }.build()
}
