package com.ciandt.book.seeker.data.repository

import com.ciandt.book.seeker.data.model.book.BookResult

interface BookRepository {
    suspend fun getBookByTerm(term: String): List<BookResult>
    fun saveHistory(query: String)
    fun getHistory(): List<String>?
}
