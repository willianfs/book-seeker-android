package com.ciandt.book.seeker.data.model.book

import com.google.gson.annotations.SerializedName

data class BookResponse(
    @SerializedName("resultCount") val resultCount: Int,
    @SerializedName("results") val results: List<BookResult>
)

data class BookResult(
    @SerializedName("artistName") val artistName: String,
    @SerializedName("trackName") val trackName: String,
    @SerializedName("artworkUrl100") val artworkUrl: String,
    @SerializedName("description") val description: String,
    @SerializedName("formattedPrice") val price: String? = null
)

fun BookResult.toBook(): Book {
    return Book(
        this.artistName,
        this.trackName,
        this.artworkUrl,
        this.description,
        this.price
    )
}
