package com.ciandt.book.seeker.ui.books.search

import com.ciandt.book.seeker.data.model.book.Book

sealed class BookViewState {
    data class Books(val books: List<Book>) : BookViewState()
    object EmptyList : BookViewState()
}
