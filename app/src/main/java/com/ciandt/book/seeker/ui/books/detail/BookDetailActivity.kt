package com.ciandt.book.seeker.ui.books.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import coil.api.load
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.data.model.book.Book
import kotlinx.android.synthetic.main.activity_book_details.*

class BookDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_details)
        setupToolbar()

        intent.getParcelableExtra<Book>(INTENT_BOOK)?.let { book ->
            setupBookDetail(book)
        }
    }

    private fun setupBookDetail(book: Book) {
        with(book) {
            imageViewArtWork.load(artworkUrl)
            textViewTitle.text = trackName
            textViewArtistName.text = artistName
            textViewDescription.text = description.removeTagHtml()
            textViewPrice.text = price
        }
    }

    companion object {
        private const val INTENT_BOOK = "INTENT_BOOK"

        fun newIntent(context: Context, book: Book): Intent {
            return Intent(context, BookDetailActivity::class.java).apply {
                putExtra(INTENT_BOOK, book)
            }
        }
    }

    private fun setupToolbar() {
        toolbar.setTitle(R.string.book_details)
        toolbar.setNavigationOnClickListener {
            finish()
        }
    }
}
