package com.ciandt.book.seeker.ui.books.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.ciandt.book.seeker.R
import com.ciandt.book.seeker.data.model.book.Book
import kotlinx.android.synthetic.main.book_item.view.*

typealias BookClicked = (Book) -> Unit

class BookSearchAdapter(private val books: List<Book>, private val bookClicked: BookClicked) :
    RecyclerView.Adapter<BookSearchAdapter.BookViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.book_item, parent, false)
        return BookViewHolder(view)
    }

    override fun getItemCount() = books.size

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        holder.bind(books[position], bookClicked)
    }

    class BookViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val textViewArtistName = itemView.textViewArtistName
        private var textTrackName = itemView.textViewTrackName
        private var artWork = itemView.imageViewArtWork

        fun bind(book: Book, bookClicked: BookClicked) {
            textViewArtistName.text = book.artistName
            textTrackName.text = book.trackName
            artWork.load(book.artworkUrl) {
                crossfade(true)
                error(R.drawable.ic_error)
            }

            itemView.setOnClickListener { bookClicked.invoke(book) }
        }
    }
}
