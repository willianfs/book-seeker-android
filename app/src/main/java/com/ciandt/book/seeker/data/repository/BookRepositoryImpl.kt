package com.ciandt.book.seeker.data.repository

import com.ciandt.book.seeker.data.model.book.BookResult
import com.ciandt.book.seeker.data.network.APIService
import com.orhanobut.hawk.Hawk
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

class BookRepositoryImpl(private val apiService: APIService) : BookRepository {
    override fun getHistory(): List<String>? {
        return Hawk.get<List<String>>(HISTORY)
    }

    override suspend fun getBookByTerm(term: String): List<BookResult> {
        return withContext(IO) {
            apiService.getBookByTerm(term).results
        }
    }

    override fun saveHistory(query: String) {
        val history: MutableList<String> =
            Hawk.get<MutableList<String>>(HISTORY, mutableListOf()).apply {
                this.add(query)
            }
        Hawk.put(HISTORY, history.distinct())
    }

    companion object {
        const val HISTORY = "HISTORY"
    }
}
