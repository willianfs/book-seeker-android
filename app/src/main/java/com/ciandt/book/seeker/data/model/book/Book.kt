package com.ciandt.book.seeker.data.model.book

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Book(
    val artistName: String,
    val trackName: String,
    val artworkUrl: String,
    val description: String,
    val price: String? = null
) : Parcelable {

    fun String.removeTagHtml(): String {
        return Regex("<[^>]*>").replace(this, "")
    }
}
