package com.ciandt.book.seeker

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ciandt.book.seeker.data.model.book.Book
import com.ciandt.book.seeker.data.model.book.BookResult
import com.ciandt.book.seeker.data.repository.BookRepository
import com.ciandt.book.seeker.ui.books.search.BookSearchViewModel
import com.ciandt.book.seeker.ui.books.search.BookViewState
import com.ciandt.book.seeker.ui.books.search.ScreenState
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class BookSearchViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    private lateinit var bookRepository: BookRepository

    @RelaxedMockK
    private lateinit var observerBooks: Observer<BookViewState>

    @RelaxedMockK
    private lateinit var observerLoading: Observer<ScreenState>

    @RelaxedMockK
    private lateinit var observerError: Observer<ScreenState>

    private lateinit var bookSearchViewModel: BookSearchViewModel

    private val dispatcher = TestCoroutineDispatcher()

    @Before
    fun setUp() {
        Dispatchers.setMain(dispatcher)

        MockKAnnotations.init(this)

        bookSearchViewModel = BookSearchViewModel(bookRepository)

        with(bookSearchViewModel) {
            bookViewState.observeForever(observerBooks)
            with(screenState) {
                observeForever(observerLoading)
                observeForever(observerError)
            }
        }
    }

    @After
    fun tearDown() {
        with(bookSearchViewModel) {
            bookViewState.removeObserver(observerBooks)
            with(screenState) {
                removeObserver(observerLoading)
                removeObserver(observerError)
            }
        }
    }

    @Test
    fun whenTheBookResponseIsSuccessful_thenBooksShouldBePosted() {

        coEvery { bookRepository.getBookByTerm("") } returns getMockResult()

        bookSearchViewModel.searchBookByTerm("")

        verifyOrder {
            observerLoading.onChanged(ScreenState.Loading)
            observerBooks.onChanged(BookViewState.Books(getMockBook()))
        }
    }

    @Test
    fun whenDefaultSearchIsSuccessful_thenBooksShouldBePosted() {

        coEvery { bookRepository.getBookByTerm("kotlin") } returns getMockResult()

        bookSearchViewModel.fetchDefaultBooks()

        verifyOrder {
            observerLoading.onChanged(ScreenState.Loading)
            observerBooks.onChanged(BookViewState.Books(getMockBook()))
        }
    }

    @Test
    fun whenTheBookResponseIsEmpty_thenEmptyListShouldBePosted() {

        coEvery { bookRepository.getBookByTerm("") } returns emptyList()

        bookSearchViewModel.searchBookByTerm("")

        verifyOrder {
            observerLoading.onChanged(ScreenState.Loading)
            observerBooks.onChanged(BookViewState.EmptyList)
        }
    }

    @Test
    fun whenTheBookResponseFails_thenErrorShouldBePosted() {

        coEvery { bookRepository.getBookByTerm("") } throws Exception()

        bookSearchViewModel.searchBookByTerm("")

        verifyOrder {
            observerLoading.onChanged(ScreenState.Loading)
            observerError.onChanged(ScreenState.Error)
        }
    }

    private fun getMockResult(): List<BookResult> {
        return listOf(
            BookResult(
                "Foo",
                "Bar",
                "foo.bar",
                "foo bar",
                "0.00"
            )
        )
    }

    private fun getMockBook(): List<Book> {
        return listOf(
            Book(
                "Foo",
                "Bar",
                "foo.bar",
                "foo bar",
                "0.00"
            )
        )
    }
}
